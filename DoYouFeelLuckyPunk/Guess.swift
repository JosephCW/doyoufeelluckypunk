//
//  Guess.swift
//  DoYouFeelLuckyPunk
//
//  Created by Watts,Joseph C on 2/21/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

struct Guess{
    var correctAnswer:Int
    var numberGuesses:Int
}
