//
//  SecondViewController.swift
//  DoYouFeelLuckyPunk
//
//  Created by Watts,Joseph C on 2/21/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Any time the tab is clicked on
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Reload the data for the table displaying the results.
        tableView.reloadData()
    }
    
    // Number of items in the table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Guesser.shared.previousGuesses.count
    }
    
    // Load the values into the table view rows
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCells")!
        let guessAtIndex = Guesser.shared.previousGuesses[indexPath.row]
        cell.textLabel?.text = "Correct Answer: \(guessAtIndex.correctAnswer)"
        cell.detailTextLabel?.text = "Number of Guesses: \(guessAtIndex.numberGuesses)"
        return cell
    }
}
