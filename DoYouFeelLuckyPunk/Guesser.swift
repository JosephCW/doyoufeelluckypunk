//
//  GuessHistorySingleton.swift
//  DoYouFeelLuckyPunk
//
//  Created by Watts,Joseph C on 2/21/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

class Guesser {
    enum answerFeedback: String {
        case low = "Your guess is too low!"
        case high = "Your guess is too high!"
        case exact = "Your guess was exact!"
    }
    
    static let shared = Guesser()
    private var _currentCorrectAnswer:Int = 0
    private var _currentNumberOfAttempts:Int = 0
    private var _previousGuesses: [Guess] = []
    var previousGuesses: [Guess] {
        get {
            return _previousGuesses
        }
    }
    
    func addCurrentGameToHistory() {
        _previousGuesses.append(Guess(correctAnswer: _currentCorrectAnswer, numberGuesses: _currentNumberOfAttempts))
    }
    
    func clearPreviousGames() {
        _previousGuesses.removeAll()
    }
    
    func startNewRound() {
        _currentCorrectAnswer = Int.random(in: 1...10)
        _currentNumberOfAttempts = 0
    }
    
    func makeAGuess(_ guess:Int) -> answerFeedback {
        _currentNumberOfAttempts += 1
        if guess > _currentCorrectAnswer {
            return answerFeedback.high
        } else if guess < _currentCorrectAnswer {
            return answerFeedback.low
        } else {
            return answerFeedback.exact
        }
    }
    
    private init(){}
}
